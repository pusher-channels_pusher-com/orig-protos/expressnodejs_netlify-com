
//= gitpod /workspace/expressnodejs_netlify-com (master) $
//
//> curl http://localhost:3000/users
//
//< respond with a resource

"use strict";
// First, run 'npm install pusher express cookie-parser'
// Then run this file with 'node thisfile.js'
const path = require("path");
const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const Pusher = require("pusher");
/*
// First install the dependencies:
// npm install pusher express cors
const express = require("express");
const Pusher = require("pusher");
/**/
const my_html = ""
    + "<!DOCTYPE \"html\" -->"
    + "<html>"
    + "  <body>"
    + "    <div id=\"user_list\"></div>"
    + "    <script src=\"https://js.pusher.com/8.0.1/pusher.min.js\"></script>"
    + "    <script>"
    + "//      Pusher.logToConsole = true;"
    + "      const pusher = new Pusher("
    + "        \"APP_KEY\", // Replace with 'key' from dashboard"
    + "        {"
    + "          cluster: \"APP_CLUSTER\", // Replace with 'cluster' from dashboard"
    + "          forceTLS: true,"
    + "          channelAuthorization: {"
    + "            endpoint: \"APP_ENDPOINT/pusher/auth\","
    + "          }"
    + "        }"
    + "      );"
    + "      if (!document.cookie.match(\"(^|;) ?user_id=([^;]*)(;|$)\")) {"
    + "        // Primitive authorization! This 'user_id' cookie is read by your authorization endpoint,"
    + "        // and used as the user_id in the subscription to the 'presence-quickstart'"
    + "        // channel. This is then displayed to all users in the user list."
    + "        // In your production app, you should use a secure authorization system."
    + "        document.cookie = \"user_id=\" + \"Hywel\" // + prompt(\"Your initials:\");"
    + "      }"
    + "      const channel = pusher.subscribe(\"PIE_CHANNEL\");"
    + "      const hashCode = (s) =>"
    + "        s.split(\"\").reduce((a, b) => {"
    + "          a = (a << 5) - a + b.charCodeAt(0);"
    + "          return a & a;"
    + "        }, 0);"
    + "      function addMemberToUserList(memberId) {"
    + "        userEl = document.createElement(\"div\");"
    + "        userEl.id = \"user__\" + memberId;"
    + "        userEl.innerText = memberId;"
    + "        userEl.style.backgroundColor ="
    + "          \"hsl(\" + (hashCode(memberId) % 360) + \",70%,60%)\";"
    + "        document.getElementById(\"user_list\").appendChild(userEl);"
    + "      }"
    + "      channel.bind(\"pusher:subscription_succeeded\", () =>"
    + "        channel.members.each((member) => addMemberToUserList(member.id))"
    + "      );"
    + "      channel.bind(\"pusher:member_added\", (member) =>"
    + "        addMemberToUserList(member.id)"
    + "      );"
    + "      channel.bind(\"pusher:member_removed\", (member) => {"
    + "        const userEl = document.getElementById(\"user__\" + member.id);"
    + "        userEl.parentNode.removeChild(userEl);"
    + "      });"
    + ""
    + "    </script>"
    + "    <style>"
    + "      body {"
    + "        margin: 1em;"
    + "      }"
    + "      #user_list div {"
    + "        float: right;"
    + "        margin-left: -12px;"
    + "        font-family: sans-serif;"
    + "        text-align: center;"
    + "        height: 40px;"
    + "        width: 40px;"
    + "        line-height: 40px;"
    + "        border-radius: 50%;"
    + "        border: 3px solid white;"
    + "        color: white;"
    + "      }"
    + "    </style>"
    + "  </body>"
    + "</html>"
    + "";
var secret = "not set yet";
var pusher = null;
const new_pusher = function (new_secret) {
    if (!pusher) { }
    else
        return pusher;
    const it = new Pusher({
        //appId: "APP_ID", // Replace with 'app_id' from dashboard
        appId: "1736956",
        //key: "APP_KEY", // Replace with 'key' from dashboard
        key: "1158400e38747b789cba",
        //secret: "APP_SECRET", // Replace with 'secret' from dashboard
        secret: new_secret ? secret = new_secret : secret,
        //cluster: "APP_CLUSTER", // Replace with 'cluster' from dashboard
        cluster: "ap2",
        useTLS: true
    });
    pusher = it;
    return it;
};
/**/
const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.get("/", (req, res) => {
    req;
    res.sendFile(path.join(__dirname, "./index.html"));
});
app.get("/list", (req, res) => {
    req;
    const html = my_html
        .replace(/APP_KEY/g, "1158400e38747b789cba")
        .replace(/APP_CLUSTER/g, "ap2")
        .replace(/APP_ENDPOINT/g, "https://clinquant-paprenjak-df348c.netlify.app")
        .replace(/PIE_CHANNEL/g, "prescence-quickstart");
    res.send(html);
});
app.get("/rise", (req, res) => {
    req;
    //  res.sendFile(path.join(__dirname, "./rise.html")); return;
    const html = ""
        + "<!DOCTYPE \"html\" -->"
        + "<html>"
        + "  <body>"
        + "    <form action=\"/pusher/rise\" method=\"POST\">"
        + "<label for=\"secret\">Your secret:</label><br>"
        + "<input type=\"text\" id=\"secret\" name=\"secret\" value=\"MySecret\" /><br>"
        + "<input type=\"submit\" value=\"TRY IT!\" /><br>"
        + "  </form>"
        + "  </body>"
        + "</html>"
        + "";
    res.send(html);
});
/*\ \*/
app.post("/pusher/rise", (req, res) => {
    secret = req.body.secret.toString();
    const pusher = new_pusher();
    const result = "-" + pusher.toString().substring(2, 3);
    const authResponse = pusher.authorizeChannel("123.456", "presence-devnull"
        + "-withsecret-" + secret + result, pusher);
    res.send(authResponse);
});
app.post("/pusher/auth", (req, res) => {
    const socketId = req.body.socket_id;
    const channel = req.body.channel_name;
    // Primitive auth: the client self-identifies. In your production app,
    // the client should provide a proof of identity, like a session cookie.
    const user_id = req.cookies.user_id;
    if (pusher) { }
    else
        return;
    const presenceData = { user_id };
    const authResponse = pusher.authorizeChannel(socketId, channel, presenceData);
    res.send(authResponse);
});
/*
app.post("/pusher/auth", (req, res) => {
  const socketId = req.body.socket_id;
  const channel = req.body.channel_name;
  // This authenticates every user. Don't do this in production!
  const authResponse = pusher.authorizeChannel(socketId, channel);
  res.send(authResponse);
});
/**/
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on port ${port}!`));
/**/
/*\ \*/ /*\ \*/ /*\ \*/
/**/
